
#--------------------------------------------------------------------------------------------------------------------------------------

import uasyncio as asyncio
from tinyweb import tinyweb
import urequests
import utime
import ntptime
from machine import Pin, RTC
import onewire, ds18x20

#--------------------------------------------------------------------------------------------------------------------------------------
# Gloabal Variables
#--------------------------------------------------------------------------------------------------------------------------------------

# UTC
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}
# SENSOR
Temp = 0
# Webhook
event = 'weather_station'
api_key = 'pT3jt3e-3ICafdPstd1bIaG21MMQ4-eZFKRqso8DbWZ'

#--------------------------------------------------------------------------------------------------------------------------------------
# CooRoutines
#--------------------------------------------------------------------------------------------------------------------------------------

# Create web server application
app = tinyweb.webserver()


def web_page():
    
    global Temp
    
    # autorefresh: <meta http-equiv="refresh" content="3">
    
    html = """<!DOCTYPE HTML><html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <style>
        html {
         font-family: Arial;
         display: inline-block;
         margin: 0px auto;
         text-align: center;
        }
        h2 { font-size: 2.8rem; }
        h3 { font-size: 1.4rem; vertical-align:middle;}
        p { font-size: 2.8rem; text-align: center;}
        .units { font-size: 1.2rem; }
        .dht-labels{
          font-size: 1.5rem;
          vertical-align:middle;
          padding-bottom: 15px;
        }
      </style>
    </head>
    <body>
      <h2>
        Weather Station
      </h2>
      <h3>
        """+ 'UTC+%d: %d-%02d-%02d %02d:%02d:%02d' % (utc['timezone'], utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']) +"""
      </h3>      
      <p>
        <i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
        <span class="dht-labels">Temperature</span> 
        <span id="temperature">"""+ str(Temp) +"""</span>
        <sup class="units">&deg;C</sup>
      </p>
      <p>
      <a href="/history">history</a>
      </p>
    </body></html>"""
    return html


# Index page
@app.route('/')
async def index(request, response):
    # Start HTTP response with content-type text/html
    await response.start_html()
    # Send actual HTML page    
    await response.send(web_page())

# HTTP redirection
@app.route('/redirect')
async def redirect(request, response):
    # Start HTTP response with content-type text/html
    await response.redirect('/')


# Another one, more complicated page
@app.route('/history')
async def history(request, response):
    # Start HTTP response with content-type text/html
    
    global Temp
    
    Temp_history_h = [0,1,2,3,4,5,6,7,8,9,10,11]
    Temp_history_t = [10,11,12,13,14,15,16,17,18,19,20,21]
    
    await response.start_html()
    await response.send('<html><body><h1>Temp History</h1>'
                        '<table border=1 width=400>'
                        '<tr><td>Time</td><td>Temp, C</td></tr>')
    for i in range(12):
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(i, Temp_history_t[i]))
    await response.send('</table>'
                        '</html>')


async def CoRoutine_Time():
    """CoRoutine_Time()"""
    
    print('Start CoRoutine_Time()')
    
    ntptime.settime()
    rtc = RTC()
    
    tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)
    tm = tm[0:3] + (0,) + tm[3:6] + (0,)
    rtc.datetime(tm)
    
    while True:
        print('Time()->', end=' ')
        
        utc_time_list = utime.localtime(utime.time())
    
        utc['year']   = utc_time_list[0]
        utc['month']  = utc_time_list[1]
        utc['day']    = utc_time_list[2]
        utc['hour']   = utc_time_list[3]
        utc['minute'] = utc_time_list[4]
        utc['second'] = utc_time_list[5]
#         for key in utc:
#             for i in utc_time_list:
#                 utc[key] = utc_time_list[i]

        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
            utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
        
        await  asyncio.sleep(1)


async def CoRoutine_Temp():
    """CoRoutine_Temp()"""
    
    print('Start CoRoutine_Temp()')
    
    global Temp
        
    ds_power = Pin(23, Pin.OUT)    
    ds_power.value(1)
    ds_sensor = ds18x20.DS18X20(onewire.OneWire(Pin(22)))
    
    while True:                
        print('Temp()->', end=' ')
        
        roms = ds_sensor.scan()        
        #print(roms[0], end=': ')
        ds_sensor.convert_temp()
        utime.sleep_ms(750)
        Temp = round(ds_sensor.read_temp(roms[0]), 1) 
        print('%.1f C' % Temp)
        
        await  asyncio.sleep(10)
        
        
async def CoRoutine_Webhooks():
    """CoRoutine_Wenhooks()"""
    
    print('Start CoRoutine_Wemhooks()')
    
    global utc, Temp    
    
    while True:
        print('Webhooks()->', end=' ')
        
        try:
            #time_list = '{:02d}:{:02d}:{:02d}'.format(utc['hour'], utc['minute'], utc['second'])
            #sensor_readings = {'value1':time_list, 'value2':Temp}
            sensor_readings = {'value1':Temp}
            print(sensor_readings, end=' ')

            request_headers = {'Content-Type': 'application/json'}

            request = urequests.post(
            'http://maker.ifttt.com/trigger/' + event + '/with/key/' + api_key,
            json=sensor_readings,
            headers=request_headers)
            print(request.text)
            request.close()

        except OSError as e:
            print('Failed to read/publish sensor readings.')
            
        await  asyncio.sleep(60)
        
        
#--------------------------------------------------------------------------------------------------------------------------------------
# Main Loop        
#--------------------------------------------------------------------------------------------------------------------------------------        
        
if __name__ == '__main__':
    
    print('Start Main Loop:')
    
    loop = asyncio.get_event_loop()
    loop.create_task ( CoRoutine_Time() )
    loop.create_task ( CoRoutine_Temp() )
    loop.create_task ( CoRoutine_Webhooks() )
    
    app.run(host='0.0.0.0')
    
    # To test your server:
    # - Terminal:
    #   $ curl http://localhost:8081
    #   or
    #   $ curl http://localhost:8081/table
    #
    # - Browser:
    #   http://localhost:8081
    #   http://localhost:8081/table
    #
    # - To test HTTP redirection:
    #   curl http://localhost:8081/redirect -v


#--------------------------------------------------------------------------------------------------------------------------------------
# Example from Tinyweb project
#--------------------------------------------------------------------------------------------------------------------------------------        
"""
# Index page
@app.route('/')
async def index(request, response):
    # Start HTTP response with content-type text/html
    await response.start_html()
    # Send actual HTML page
    await response.send('<html><body><h1>Hello, world! (<a href="/table">table</a>)</h1></html>\n')
    

# HTTP redirection
@app.route('/redirect')
async def redirect(request, response):
    # Start HTTP response with content-type text/html
    await response.redirect('/')


# Another one, more complicated page
@app.route('/table')
async def table(request, response):
    # Start HTTP response with content-type text/html
    
    global Temp
    
    await response.start_html()
    await response.send('<html><body><h1>Simple table</h1>'
                        '<table border=1 width=400>'
                        '<tr><td>Name</td><td>Value</td></tr>')
    for i in range(10):
        await response.send('<tr><td>Name{}</td><td>Value{}</td></tr>'.format(i, i))            
    await response.send('</table>'
                        '</html>')
"""
#--------------------------------------------------------------------------------------------------------------------------------------        