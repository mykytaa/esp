
from time import sleep

try:
    import usocket as socket
except:
    import socket

import urequests

import gc
gc.collect()

import utime
import ntptime
import uasyncio as asyncio

import machine
from machine import Pin, RTC
import onewire, ds18x20

event = 'weather_station'
api_key = 'pT3jt3e-3ICafdPstd1bIaG21MMQ4-eZFKRqso8DbWZ'

# UTC
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}

utc_time_list = ()
utc_year = 0
utc_month = 0
utc_day = 0
utc_hour = 0
utc_minute = 0
utc_second = 0
# SENSOR
Temp = 0

################################################################################
# CoRoutines
################################################################################
        
async def CoRoutine_Time():
    """CoRoutine_Time()"""
    
    ntptime.settime()
    rtc = RTC()
    
    tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)
    tm = tm[0:3] + (0,) + tm[3:6] + (0,)
    rtc.datetime(tm)
    
    while True:
        print('CoRoutine_Time()->', end=' ')
        
        utc_time_list = utime.localtime(utime.time())
    
        utc['year']   = utc_time_list[0]
        utc['month']  = utc_time_list[1]
        utc['day']    = utc_time_list[2]
        utc['hour']   = utc_time_list[3]
        utc['minute'] = utc_time_list[4]
        utc['second'] = utc_time_list[5]
#         for key in utc:
#             for i in utc_time_list:
#                 utc[key] = utc_time_list[i]

        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
            utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
        
        await  asyncio.sleep(1)


async def CoRoutine_Temp():
    """CoRoutine_Temp()"""
    global Temp
    
    ds_power = Pin(23, Pin.OUT)    
    ds_power.value(1)
    ds_sensor = ds18x20.DS18X20(onewire.OneWire(Pin(22)))
    
    while True:                
        print('CoRoutine_Temp()->', end=' ')
        
        roms = ds_sensor.scan()        
        #print(roms[0], end=': ')
        ds_sensor.convert_temp()
        utime.sleep_ms(750)
        Temp = round(ds_sensor.read_temp(roms[0]), 1) 
        print('%d C' % Temp)
        
        await  asyncio.sleep(10) 


async def CoRoutine_Webhooks():
    """CoRoutine_Wenhooks()"""
    global Temp
    global utc
    
    while True:
        print('CoRoutine_Wemhooks()->', end=' ')
        
        try:
            time_list = '{:02d}:{:02d}:{:02d}'.format(utc['hour'], utc['minute'], utc['second'])
            sensor_readings = {'value1':time_list, 'value2':Temp}
            print(sensor_readings, end=' ')

            request_headers = {'Content-Type': 'application/json'}

            request = urequests.post(
            'http://maker.ifttt.com/trigger/' + event + '/with/key/' + api_key,
            json=sensor_readings,
            headers=request_headers)
            print(request.text)
            request.close()

        except OSError as e:
            print('Failed to read/publish sensor readings.')
            
        await  asyncio.sleep(60)
        

def web_page1():
    """web_page()"""
    
    file = open("index.html", "r")
    page = file.read()
    file.close()        
    return page


def web_page():
    
    global Temp
    
    html = """<!DOCTYPE HTML><html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="refresh" content="3">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <style>
        html {
         font-family: Arial;
         display: inline-block;
         margin: 0px auto;
         text-align: center;
        }
        h2 { font-size: 2.8rem; }
        h3 { font-size: 1.4rem; vertical-align:middle;}
        p { font-size: 2.8rem; text-align: center;}
        .units { font-size: 1.2rem; }
        .dht-labels{
          font-size: 1.5rem;
          vertical-align:middle;
          padding-bottom: 15px;
        }
      </style>
    </head>
    <body>
      <h2>
        Weather Station
      </h2>
      <h3>
        """+ 'UTC+%d: %d-%02d-%02d %02d:%02d:%02d' % (utc['timezone'], utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']) +"""
      </h3>      
      <p>
        <i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
        <span class="dht-labels">Temperature</span> 
        <span id="temperature">"""+ str(Temp) +"""</span>
        <sup class="units">&deg;C</sup>
      </p>
    </body></html>"""
    return html


async def CoRoutine_Web():
    """CoRoutine_Web()"""
    
    try:    
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('', 80))
        s.listen(5)
    except OSError as e:
        machine.reset()
    
    while True:
        print('CoRoutine_Web()->', end=' ')
        
        try:
            if gc.mem_free() < 102000:
                gc.collect()
            conn, addr = s.accept()
            conn.settimeout(3.0)
            print('Got a connection from %s' % str(addr))
            request = conn.recv(1024)
            conn.settimeout(None)
            request = str(request)
            #print('Content = %s' % request)
#             response = web_page() % (UTC_TIMEZONE, utc_year, utc_month, utc_day, utc_hour, utc_minute, utc_second,
#                                      str(bme.temperature), str(bme.humidity), str(bme.pressure), str(bme.pressure_mm))
            response = web_page()
            conn.send('HTTP/1.1 200 OK\n')
            conn.send('Content-Type: text/html\n')
            conn.send('Connection: close\n\n')
            conn.sendall(response)
            conn.close()
        except OSError as e:
            conn.close()
            print('\tConnection closed')
        
        await  asyncio.sleep(10)

################################################################################
# Main Loop
################################################################################

loop = asyncio.get_event_loop()
loop.create_task ( CoRoutine_Time() )
loop.create_task ( CoRoutine_Temp() )
loop.create_task ( CoRoutine_Web() )
loop.create_task ( CoRoutine_Webhooks() )
loop.run_forever()
