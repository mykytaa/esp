#================================================================================#
# main.py
# 
#================================================================================#

import machine
from machine import Pin, I2C, UART, RTC
import BME280

import wifimgr
import utime
import ntptime
#import ntptime_userdef
import uasyncio as asyncio
import urequests
import ujson
from tinyweb import tinyweb

#-------------------------------------------------------------------------------#
# Gloabal Variables
#-------------------------------------------------------------------------------#

# UTC
utc_time_list = (0,0,0,0,0,0)
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}
# JSON
Module_Parameter = {
    'Modules': {
        'IOCore': 0,
        'Dpm': 0,
        'Ccs': 0,
        'Chademo': 0,
        'Gbt': 0,
        'J1772': 0,
        'Schuko': 0,
        'Measure': 0,
    },
    'IOCore': {        
        
    },
    'Dpm': {
     
    },
    'Ccs': {
     
    },
    'Chademo': {
     
    },
    'Gbt': {
     
    },
    'J1772': {
     
    },    
    'Schuko': {
     
    },
    'Measure': {
     
    },
}


#-------------------------------------------------------------------------------#
# Init Peripherals
#-------------------------------------------------------------------------------#

# ESP32 - Pin assignment
i2c = I2C(scl=Pin(19), sda=Pin(23), freq=10000)
sensor_power = Pin(22, Pin.OUT)

# USART
#uart = UART(1, tx=33, rx=25)
uart = UART(1, tx=14, rx=12)
uart.init(115200, bits=8, parity=None, timeout=10)
#uart_power = Pin(26, Pin.OUT)
#uart_power.value(0)
# BME, power ON
sensor_power.value(0)
bme = BME280.BME280(i2c=i2c)

# Create web server application
app = tinyweb.webserver()

#-------------------------------------------------------------------------------#
# Functions
#-------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

def TimeSync():
    """
    Time sync from www
    """
    #print('Start Func_TimeSync()')
    
    try:
        print('ntptime sync->', end=' ')
        
        ntptime.settime()
        tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)        
        RTC().datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))                
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
                utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
        return True
    except Exception:
        print('Error.')
        return False


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

def TimeGet():
    """
    Time get from rtc
    """
    #print('Start Func_TimeGet()')
    
    tm = utime.localtime(utime.time())
    
    utc['year']   = tm[0]
    utc['month']  = tm[1]
    utc['day']    = tm[2]
    utc['hour']   = tm[3]
    utc['minute'] = tm[4]
    utc['second'] = tm[5]
    
    #print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
    #utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
    
    return ('{:02d}:{:02d}:{:02d}'.format(utc['hour'], utc['minute'], utc['second']))


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

async def aFunc_Time():
    """
    Read Data & Time from www
    """
    print('Start Func_Time()')
      
    TimeSync()
    
    while(True):
        
        #print('Time()->', end=' ')
        
        tm = utime.localtime(utime.time())
        
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
        
        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
        utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
           
        await  asyncio.sleep(1)
       
     
#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#
        
async def aFunc_Uart_Tx():
    """Send time % weather to uart.
    """
    print('Start Func_Uart()')
    
    while(True):
                                
        msg = 'Temperature =%.1f C, Humidity =%.1f %%, Presure =%.1f hPa (%d mmHg)' % (bme.temperature_raw, bme.humidity_raw, bme.pressure_raw, bme.pressure_mm_raw)        
        time = '%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])        
        print('%s Uart_Tx: ' % TimeGet() + msg)
                
        await  asyncio.sleep(1)
     
     
def JsonPars(data):
    """
    JSON passed object
    """
    #print('Start JsonPars()')
      
    try:
        print('\t Json_pars:', end=' ')
        for module in data:            
            for module1 in Module_Parameter:
                if(module == module1):
                    Module_Parameter['Modules'][module] = 1
                    #print(Module_Parameter['Modules'])
                    Module_Parameter[module] = data[module]
                    print('%s:' % (module), end=' ')
                    for key in Module_Parameter[module]:
                        #Module_Parameter[module][key] = int(Module_Parameter[module][key])
                        print('%s=%s,' % (key, Module_Parameter[module][key]), end=' ')            
        print('\r')
    except Exception:
        print('error')


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

async def aFunc_Uart_Rx():
    """Receive data line (ended='\n') from uart.
    """
    print('Start aFunc_Uart_Rx()')
    #uart.write('Start aFunc_Uart_Rx1()')
    
    sreader = asyncio.StreamReader(uart)
    while True:
        res = await sreader.readline()
        print('%s Uart_Rx:' % TimeGet(), end=' ')
        print(res)
        
        try:
            js = ujson.loads(res)
            #print(js)
            JsonPars(js)
        except Exception:
            print('json err')
        
        #await  asyncio.sleep_ms (1)
        

def page_index():
    """
    """    
    present=[]
    for module in Module_Parameter['Modules']:
        if Module_Parameter['Modules'][module] == 1:
            present.append(module)
    
    # autorefresh: <meta http-equiv="refresh" content="3">            
    html = """
    <!DOCTYPE html>
    <html>
      <meta http-equiv="refresh" content="3">
      <head>
        <h1>Main display</h1> 
      </head>
        <body>
          <h3>
          """+ 'UTC+%d: %d-%02d-%02d %02d:%02d:%02d' % (utc['timezone'], utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']) +"""
          </h3>          
          <p>
          """+'Modules in system: %s' % (present) +"""
          </p>
          <p>
            <a href="/IOCore">IOCore</a> <a href="/Dpm">Dpm</a> <a href="/Ccs">Ccs</a> <a href="/Chademo">Chademo</a> <a href="/Gbt">Gbt</a>
            <a href="/J1772">J1772</a> <a href="/Schuko">Schuko</a> <a href="/Measure">Measure</a>
          </p>
        </body>
    </html>"""
    return html


# Index page
@app.route('/')
async def index(request, response):
    # Start HTTP response with content-type text/html
    await response.start_html()
    # Send actual HTML page    
    await response.send(page_index())

# HTTP redirection
@app.route('/redirect')
async def redirect(request, response):
    # Start HTTP response with content-type text/html
    await response.redirect('/')


# Another one, more complicated page
@app.route('/IOCore')
async def io_core(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>IOCore</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["IOCore"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["IOCore"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')

@app.route('/Dpm')
async def dpm(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>DPM</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["Dpm"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Dpm"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')

@app.route('/Ccs')
async def ccs(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>CCS</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["Ccs"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Ccs"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')
    
@app.route('/Chademo')
async def chademo(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>CHAdeMO</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["Chademo"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Chademo"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')    
    
@app.route('/Gbt')
async def gbt(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>GB/t</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["Gbt"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Gbt"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')
    
@app.route('/J1772')
async def j1772(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>J1772</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["J1772"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["J1772"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')

@app.route('/Schuko')
async def schuko(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>Schuko</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["Schuko"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Schuko"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')

@app.route('/Measure')
async def measure(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()
    await response.send('<html><meta http-equiv="refresh" content="3"><body><h1>Measure</h1>'
                        '<p>Time: {:02d}:{:02d}:{:02d}</p>'.format(utc['hour'], utc['minute'], utc['second']))
    await response.send('<table border=1 width=400>')
    for key in Module_Parameter["Measure"]:
        await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Measure"][key]))
    await response.send('</table>')
    await response.send('<p></p><a href="/">back</a>')
    await response.send('</html>')
    
    
#===============================================================================#
# Main Loop
#===============================================================================#

if __name__ == '__main__':
    print('Start Main:')
    
    #TimeSync()    
    loop  =  asyncio.get_event_loop ()
    loop.create_task ( aFunc_Time())
    loop.create_task ( aFunc_Uart_Tx())    
    loop.create_task(aFunc_Uart_Rx())    
    #loop.run_forever ()
    app.run(host='0.0.0.0')
