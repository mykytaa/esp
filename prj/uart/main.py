#================================================================================#
# main.py
# 
#================================================================================#

import machine
from machine import Pin, I2C, UART, RTC
import BME280

import wifimgr
import utime
import ntptime
#import ntptime_userdef
import uasyncio as asyncio
import urequests
import ujson

#-------------------------------------------------------------------------------#
# Gloabal Variables
#-------------------------------------------------------------------------------#

# UTC
utc_time_list = (0,0,0,0,0,0)
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}
# Webhook
event = 'weather_station'
api_key = 'pT3jt3e-3ICafdPstd1bIaG21MMQ4-eZFKRqso8DbWZ'

#-------------------------------------------------------------------------------#
# Init Peripherals
#-------------------------------------------------------------------------------#

# ESP32 - Pin assignment
i2c = I2C(scl=Pin(19), sda=Pin(23), freq=10000)
sensor_power = Pin(22, Pin.OUT)

# USART
#uart = UART(1, tx=33, rx=25)
uart = UART(1, tx=14, rx=12)
uart.init(115200, bits=8, parity=None, timeout=10)
#uart_power = Pin(26, Pin.OUT)
#uart_power.value(0)
# BME, power ON
sensor_power.value(0)
bme = BME280.BME280(i2c=i2c)

  
#-------------------------------------------------------------------------------#
# Functions
#-------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

def TimeSync():
    """
    Time sync from www
    """
    #print('Start Func_TimeSync()')
    
    try:
        print('ntptime sync->', end=' ')
        
        ntptime.settime()
        tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)        
        RTC().datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))                
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
                utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
        return True
    except Exception:
        print('Error.')
        return False


async def aFunc_Time():
    """
    Read Data & Time from www
    """
    print('Start Func_Time()')
      
    TimeSync()
    
    while(True):
        
        #print('Time()->', end=' ')
        
        tm = utime.localtime(utime.time())
        
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
        
        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
        utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
           
        await  asyncio.sleep(1)


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

def TimeGet():
    """
    Time get from rtc
    """
    #print('Start Func_TimeGet()')
    
    tm = utime.localtime(utime.time())
    
    utc['year']   = tm[0]
    utc['month']  = tm[1]
    utc['day']    = tm[2]
    utc['hour']   = tm[3]
    utc['minute'] = tm[4]
    utc['second'] = tm[5]
    
    #print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
    #utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
    
    return ('{:02d}:{:02d}:{:02d}'.format(utc['hour'], utc['minute'], utc['second']))
       
     
#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#
        
async def aFunc_Uart_Tx():
    """Send time % weather to uart.
    """
    print('Start Func_Uart()')
    
    while(True):
                                
        msg = 'Temperature =%.1f C, Humidity =%.1f %%, Presure =%.1f hPa (%d mmHg)' % (bme.temperature_raw, bme.humidity_raw, bme.pressure_raw, bme.pressure_mm_raw)        
        time = '%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])        
        print('%s Uart_Tx: ' % TimeGet() + msg)
                
        await  asyncio.sleep(1)
     
 
#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#
        
async def aFunc_Uart_Rx1():
    """Receive data line from uart.
    """
    print('Start aFunc_Uart_Rx1()')
    uart.write('Start aFunc_Uart_Rx1()')
    
    while(True):
                
        #if(uart.any == True):
        buf = uart.readline()
        if(buf is not None):
            print('%s Uart_Rx:' % TimeGet(), end=' ')
            print(buf)
    
        await  asyncio.sleep_ms (10)


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

async def aFunc_Uart_Rx():
    """Receive data line (ended='\n') from uart.
    """
    print('Start aFunc_Uart_Rx()')
    #uart.write('Start aFunc_Uart_Rx1()')
    
    sreader = asyncio.StreamReader(uart)
    while True:
        res = await sreader.readline()
        print('%s Uart_Rx:' % TimeGet(), end=' ')
        print(res)
        try:
            js = ujson.loads(res)
            print(js)
        except ValueError:
            pass
        
        #await  asyncio.sleep_ms (1)
        
        
#===============================================================================#
# Main Loop
#===============================================================================#

if __name__ == '__main__':
    print('Start Main:')
    
    #TimeSync()    
    loop  =  asyncio.get_event_loop ()
    loop.create_task ( aFunc_Time())
    loop.create_task ( aFunc_Uart_Tx())    
    loop.create_task(aFunc_Uart_Rx())    
    loop.run_forever ()
