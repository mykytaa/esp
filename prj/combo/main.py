#================================================================================#
# main.py
# 
#================================================================================#

import machine
from machine import Pin, I2C, UART, RTC

import utime
import ntptime
import uasyncio as asyncio
import urequests
import ujson
import tinyweb
from ucollections import OrderedDict

#-------------------------------------------------------------------------------#
# Gloabal Variables
#-------------------------------------------------------------------------------#

# UTC
utc_time_list = (0,0,0,0,0,0)
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}
# JSON
Module_Status = OrderedDict(
{    
    'IOCore': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Dpm': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Ccs': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Chademo': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Gbt': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'J1772': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Schuko': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Measure': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    }
})
    
Module_Parameter = OrderedDict(
{    
    'IOCore': {        
        
    },
    'Dpm': {
     
    },
    'Ccs': {
     
    },
    'Chademo': {
     
    },
    'Gbt': {
     
    },
    'J1772': {
     
    },    
    'Schuko': {
     
    },
    'Measure': {
     
    },
})


#-------------------------------------------------------------------------------#
# Init Peripherals
#-------------------------------------------------------------------------------#

# ESP32 - Pin assignment

# USART
#uart = UART(1, tx=33, rx=25)
uart = UART(1, tx=14, rx=12)
uart.init(115200, bits=8, parity=None, timeout=10)
#uart_power = Pin(26, Pin.OUT)
#uart_power.value(0)

# Create web server application
app = tinyweb.webserver()

#-------------------------------------------------------------------------------#
# Functions
#-------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

def TimeSync():
    """Time sync from www
    """
    #print('Start Func_TimeSync()')
    
    try:
        print('ntptime sync->', end=' ')
        
        ntptime.settime()
        tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)        
        RTC().datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))                
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
                utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
        return True
    except Exception:
        print('Error.')
        return False


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

def TimeGet():
    """Time get from rtc
    """
    #print('Start Func_TimeGet()')
    
    tm = utime.localtime(utime.time())
    
    utc['year']   = tm[0]
    utc['month']  = tm[1]
    utc['day']    = tm[2]
    utc['hour']   = tm[3]
    utc['minute'] = tm[4]
    utc['second'] = tm[5]
    
    #print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
    #utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
    
    return ('{:02d}:{:02d}:{:02d}'.format(utc['hour'], utc['minute'], utc['second']))


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

async def aFunc_Time():
    """Read Data & Time from www
    """
    print('Start Func_Time()')
      
    TimeSync()
    
    while(True):
        
        #print('Time()->', end=' ')
        
        tm = utime.localtime(utime.time())          
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
        
        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
        utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
           
        await  asyncio.sleep(1)
       

def VerifyModulesConnect():
    """
    """
    #print('Start JsonPars()')
              
    for module in Module_Status:
    
        if utime.ticks_diff(utime.ticks_ms(), Module_Status[module]['Time']) > Module_Status[module]['Timeout']:
            
            Module_Status[module]['Stat'] = 0                        
            Module_Parameter[module] = 0


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#
        
async def aFunc_Uart_Tx():
    """Send time % weather to uart.
    """
    print('Start Func_Uart()')
    
    while(True):
                                     
        print('%s Uart_Tx: ' % TimeGet() + 'Sync mesg')
        
        VerifyModulesConnect()
        #print('Module_Parameter["Measure"] =%s' % Module_Parameter['Measure'])
                
        await  asyncio.sleep(1)
     
     
def JsonPars(data):
    """JSON passed object
    """
    #print('Start JsonPars()')
      
    try:
        print('\t Json_pars:', end=' ')
        for module in data:            
            for module1 in Module_Parameter:
                if(module == module1):                    
                    Module_Status[module]['Time'] = utime.ticks_ms()
                    Module_Status[module]['Stat'] = 1                    
                    Module_Parameter[module] = data[module]
                    print('%s:' % (module), end=' ')
                    for key in Module_Parameter[module]:
                        #Module_Parameter[module][key] = int(Module_Parameter[module][key])
                        print('%s=%s,' % (key, Module_Parameter[module][key]), end=' ')            
        print('\r')
    except Exception:
        print('error')

        
#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

async def aFunc_Uart_Rx():
    """Receive data line (ended='\n') from uart.
    """
    print('Start aFunc_Uart_Rx()')
    #uart.write('Start aFunc_Uart_Rx1()')
    
    sreader = asyncio.StreamReader(uart)
    while True:
        res = await sreader.readline()
        print('%s Uart_Rx:' % TimeGet(), end=' ')
        print(res)
        try:
            js = ujson.loads(res)
            print(js)
            JsonPars(js)
        except Exception:
            print('json err')
        
        #await  asyncio.sleep_ms (1)
        

def page_index():
    """Index page
    """    
    # Calculate online modules in system
    present=[]
    for module in Module_Status:
        if Module_Status[module]['Stat'] == 1:
            present.append(module)
    
    # autorefresh: <meta http-equiv="refresh" content="3">
    
    html = """
    <html>
      <head>          
          <title>Combo Main Display</title>
          <style>
           h1 {font-size: 500%;}
           h2 {font-size: 300%;}           
           p  {font-size: 250%;}
           a  {font-size: 120%;}
          </style>
      </head>
        <body>
          <h1>Combo Main display</h1>
          <h2>"""+ 'UTC+%d: %d-%02d-%02d %02d:%02d:%02d' % (utc['timezone'], utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']) +"""</h2>
          <p>System: """+'%s' % (present) +"""</p>
          <p>AC: <a href="/IOCore">IOCore</a> <a href="/Dpm">Dpm</a> <a href="/Ccs">Ccs</a> <a href="/Chademo">Chademo</a> <a href="/Gbt">Gbt</a></p>
          <p>DC: <a href="/J1772">J1772</a> <a href="/Schuko">Schuko</a> <a href="/Measure">Measure</a></p>
          <p>    <a href="/Exp">Exp</a></p>
        </body>
    </html>"""
    return html


# Index page
@app.route('/')
async def index(request, response):
    # Start HTTP response with content-type text/html
    await response.start_html()
    # Send actual HTML page    
    await response.send(page_index())


# HTTP redirection
@app.route('/redirect')
async def redirect(request, response):
    # Start HTTP response with content-type text/html
    await response.redirect('/')


#------------------------------------------------------------------------

# Another one, more complicated page

@app.route('/Exp/led_on')
async def exp(request, response):
    await response.start_html()

@app.route('/Exp/led_off')
async def exp(request, response):
    await response.start_html()

@app.route('/Exp')
async def exp(request, response):
    # Start HTTP response with content-type text/html
    
    gpio_state = 'ON'
    
    await response.start_html()    
    await response.send('<html><head> <title>ESP Web Server</title> <meta name="viewport" content="width=device-width, initial-scale=1">'
                        '<link rel="icon" href="data:,">'
                        '<style>'
                            'html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}'
                            'h1{color: #0F3376; padding: 2vh;}'
                            'p{font-size: 1.5rem;}'
                            '.button{display: inline-block; background-color: #e7bd3b; border: none;'
                            'border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}'
                            '.button2{display: inline-block; background-color: #4286f4; border: none;'
                            'border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}'
                        '</style>'
                        '</head>'
                        '<body><h1>ESP Web Server</h1>'
                            '<p>GPIO state: <strong>ON</strong></p>'
                            '<p><a href="/Exp/led_on"><button class="button">ON </button></a></p>'
                            '<p><a href="/Exp/led_off"><button class="button2">OFF</button></a></p>'
                        '</body></html>')
    
    
#------------------------------------------------------------------------
    

# Another one, more complicated page
@app.route('/IOCore')
async def io_core(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo IOCore display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>IOCore</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["IOCore"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["IOCore"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["IOCore"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')

    
@app.route('/Dpm')
async def io_core(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo DPM display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>DPM</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Dpm"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["Dpm"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Dpm"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')


@app.route('/Ccs')
async def ccs(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo CCS display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>CCS</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Ccs"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["Ccs"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Ccs"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')
    

@app.route('/Chademo')
async def chademo(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo CHAdeMO display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>CHAdeMO</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Chademo"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["Chademo"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Chademo"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')
    
    
@app.route('/Gbt')
async def gbt(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo GB/t display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>GB/t</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Gbt"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["Gbt"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Gbt"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')  


@app.route('/J1772')
async def j1772(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo J1772 display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>J1772</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["J1772"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["J1772"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["J1772"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')


@app.route('/Schuko')
async def schuko(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo J1772 display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>Schuko</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Schuko"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["Schuko"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Schuko"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')


@app.route('/Measure')
async def measure(request, response):
    # Start HTTP response with content-type text/html
            
    await response.start_html()    
    await response.send('<html><head><title>Combo Measure display</title><style>'
                        'h1 {font-size: 500%;}'
                        'h2 {font-size: 300%;}' 
                        'p  {font-size: 300%;}'
                        'table, th, td {font-size: 200%;}'
                        '</style></head>')
    await response.send('<body><h1>Measure</h1>'
                        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))    
    if(Module_Parameter["Measure"] !=0):
        await response.send('<table border=1>')
        for key in Module_Parameter["Measure"]:
            await response.send('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Measure"][key]))
        await response.send('</table>')
    else:
        await response.send('<p>no connect</p>')
    await response.send('<p><a href="/">back</a></p>'
                        '</html>')


#===============================================================================#
# Main Loop
#===============================================================================#

if __name__ == '__main__':
    print('Start Main:')
    
    #TimeSync()    
    loop  =  asyncio.get_event_loop ()
    loop.create_task ( aFunc_Time())
    loop.create_task ( aFunc_Uart_Tx())    
    loop.create_task(aFunc_Uart_Rx())    
    #loop.run_forever ()
    app.run(host='0.0.0.0')
