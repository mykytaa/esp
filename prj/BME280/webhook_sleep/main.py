#================================================================================#
# main.py
# 
#================================================================================#

import machine
from machine import Pin, I2C, UART, RTC
import BME280

import wifimgr
import utime
import ntptime
import uasyncio as asyncio
import urequests

#-------------------------------------------------------------------------------#
# Gloabal Variables
#-------------------------------------------------------------------------------#

# UTC
utc_time_list = (0,0,0,0,0,0)
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}
# Webhook
event = 'weather_station'
api_key = 'pT3jt3e-3ICafdPstd1bIaG21MMQ4-eZFKRqso8DbWZ'

#-------------------------------------------------------------------------------#
# Init Peripherals
#-------------------------------------------------------------------------------#

# ESP32 - Pin assignment
i2c = I2C(scl=Pin(19), sda=Pin(23), freq=10000)
sensor_power = Pin(22, Pin.OUT)

# USART
uart = UART(2, 115200)
uart.init(115200, bits=8, parity=None)
# BME, power ON
sensor_power.value(0)
bme = BME280.BME280(i2c=i2c)
  
#-------------------------------------------------------------------------------#
# Functions
#-------------------------------------------------------------------------------#

def Func_TimeGetFromInet():
    """
    Read Data & Time from www and Set RTC
    """
    #print('Start Func_TimeSet()')
    
    try:
        print('TimeGetFromInet()->', end=' ')
        #ntptime_userdef.settime()
        ntptime.settime()
        
        tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)
        RTC().datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))
                
        #print(tm, end=' ')
        #utc_time_list = utime.localtime(utime.time())

        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
    #         for key in utc:
    #             for i in utc_time_list:
    #                 utc[key] = utc_time_list[i]

        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
            utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
    except Exception:
        print('Error ntptime sync.')
    
    
    
    
def Func_Time():
    """
    Read Data & Time from www
    """
    #print('Start Func_Time()')
    
    ###rtc = RTC()    
    print('Time()->', end=' ')
    
    #utc_time_list = rtc.datetime()
    tm = utime.localtime(utime.time())
    
    utc['year']   = tm[0]
    utc['month']  = tm[1]
    utc['day']    = tm[2]
    utc['hour']   = tm[3]
    utc['minute'] = tm[4]
    utc['second'] = tm[5]
#         for key in utc:
#             for i in utc_time_list:
#                 utc[key] = utc_time_list[i]

    print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
        utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
    
           
def Func_BME280():
    """Read temperature, humidity and barometric pressure.
       from BME280 sensor
    """
    #print('Start Func_BME280()')
    
    global bme              
    print('BME280()->', end=' ')

    #bme = BME280.BME280(i2c=i2c)
    
    bme.Temperature = bme.temperature_raw
    bme.Humidity    = bme.humidity_raw
    bme.Pressure    = bme.pressure_raw
    bme.Pressure_mm = bme.pressure_mm_raw
    
    #print('Temperature ={:s} C, Humidity ={:s} %, Presure ={:s} hPa, Presure ={:s} mmHg'.format(bme.temperature, bme.humidity, bme.pressure, bme.pressure_mm))
    #print('Temperature =%s C, Humidity =%s %%, Presure =%s hPa, Presure =%s mmHg' % (bme.temperature, bme.humidity, bme.pressure, bme.Pressure_mm))
    #print('Temperature ={} C, Humidity ={} %, Presure ={} hPa ({} mmHg)'.format(bme.Temperature, bme.Humidity, bme.Pressure, bme.Pressure_mm))
    print('Temperature =%.1f C, Humidity =%.1f %%, Presure =%.1f hPa (%d mmHg)' % (bme.Temperature, bme.Humidity, bme.Pressure, bme.Pressure_mm))
           
    
def Func_Webhooks():
    """
    IFTTT Webhooks service to integrate the project with Google sheets.
    google account:  weather.st.home@gmail.com
    password:        Weather_01
    """
    #print('Start Func_Webhook()')
    print('Webhooks()->', end=' ')
    
    try:
        #sensor_readings = {'value1':bme.temperature_raw, 'value2':bme.humidity_raw, 'value3':bme.pressure_raw}
        sensor_readings = {'value1':bme.Temperature, 'value2':bme.Humidity, 'value3':bme.Pressure}        
        request_headers = {'Content-Type': 'application/json'}
        request = urequests.post('http://maker.ifttt.com/trigger/' + event + '/with/key/' + api_key,
                                 json=sensor_readings, headers=request_headers)
        print(sensor_readings, end=' ')
        print(request.text)
        request.close()

    except OSError as e:
        print('Failed to read/publish sensor readings.')
    except Exception:
        print('Error send webhooks.')
        
  
#-------------------------------------------------------------------------------#
# Main Loop
#-------------------------------------------------------------------------------#

if __name__ == '__main__':
    print('Start Main:')
    
    utime.sleep(1)
    Func_BME280()
    
    if wifimgr.wlan_sta.isconnected() is False:
        print("Don't connect to wifi: machine.reset()\r\n")
        machine.reset()
    
    Func_TimeGetFromInet()

    Func_Webhooks()
        
    # 1 min
    #timeout = 1000*( (60-utc['second']))
    # 10 min
    timeout = 1000*( 60*(10 - utc['minute']%10 - 1) + (60-utc['second']))

    print('deepsleep %d ms...' % (timeout))
    machine.deepsleep(timeout)

