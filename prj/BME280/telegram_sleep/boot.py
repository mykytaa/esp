#----------------------------------------------------------------------------------------------------#
# boot.py
# Wi-fi manager
# Wi-fi name&pasw:
"""
E-line;eline111
mykyta;mykyta_325
Home;123456789101112
"""
#----------------------------------------------------------------------------------------------------#

#import esp
#esp.osdebug(None)

import gc
gc.collect()

import webrepl
webrepl.start()

import wifimgr
from utime import sleep
import machine

import ntptime_userdef

#----------------------------------------------------------------------------------------------------#
# Wi-Fi Manager
#----------------------------------------------------------------------------------------------------#

wlan = wifimgr.get_connection()
if wlan is None:
    print('Could not initialize the network connection.')
    start = utime.ticks_ms()
    while True:
        pass  # you shall not pass :D
        if(utime.ticks_ms()-start > 5*60*1000):
            machine.reset()
            
# Main Code goes here, wlan is a working network.WLAN(STA_IF) instance.
print('ESP OK')

#----------------------------------------------------------------------------------------------------#
# Start Ftp Server #
#----------------------------------------------------------------------------------------------------#

import uftpd


