#================================================================================#
# main.py
# Async CoRoutines 'time', 'web', 'webhook'
#================================================================================#

import machine
from machine import Pin, I2C, UART, RTC
import BME280

import utime
import ntptime
import uasyncio as asyncio
from tinyweb import tinyweb
import urequests

#-------------------------------------------------------------------------------#
# Gloabal Variables
#-------------------------------------------------------------------------------#

# UTC
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}
# BME280
bme = 0
# web
page = '\0'
# Webhook
event = 'weather_station'
api_key = 'pT3jt3e-3ICafdPstd1bIaG21MMQ4-eZFKRqso8DbWZ'


#-------------------------------------------------------------------------------#
# Init Peripherals
#-------------------------------------------------------------------------------#

# ESP32 - Pin assignment
i2c = I2C(scl=Pin(19), sda=Pin(23), freq=10000)
sensor_power = Pin(22, Pin.OUT)

# USART
uart = UART(2, 115200)
uart.init(115200, bits=8, parity=None)
# BME, power ON
sensor_power.value(0)
bme = BME280.BME280(i2c=i2c)
# Create web server application
app = tinyweb.webserver()

#-------------------------------------------------------------------------------#
# Web
#-------------------------------------------------------------------------------#

def web_page():
    """
    Read web-page 'index.html' from flash file system (FFS)
    """ 
    file = open("index.html", "r")
    page = file.read()
    file.close()        
    return page


# Index page
@app.route('/')
async def index(request, response):
    # formating page string
    page_frmt = page % (utc['timezone'], utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second'],
                   str(bme.temperature), str(bme.humidity), str(bme.pressure), str(bme.pressure_mm))
    
    # Start HTTP response with content-type text/html
    await response.start_html()
    # Send actual HTML page    
    await response.send(page_frmt)

# HTTP redirection
@app.route('/redirect')
async def redirect(request, response):
    # Start HTTP response with content-type text/html
    await response.redirect('/')
    
    
#-------------------------------------------------------------------------------#
# Async CoRoutines
#-------------------------------------------------------------------------------#

async def CoRoutine_Time():
    """
    CoRoutine_Time()
    """
    print('Start CoRoutine_Time()')
    
    ntptime.settime()
    rtc = RTC()
    
    tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)
    tm = tm[0:3] + (0,) + tm[3:6] + (0,)
    rtc.datetime(tm)
    
    while True:
        print('Time()->', end=' ')
        
        utc_time_list = utime.localtime(utime.time())
    
        utc['year']   = utc_time_list[0]
        utc['month']  = utc_time_list[1]
        utc['day']    = utc_time_list[2]
        utc['hour']   = utc_time_list[3]
        utc['minute'] = utc_time_list[4]
        utc['second'] = utc_time_list[5]
#         for key in utc:
#             for i in utc_time_list:
#                 utc[key] = utc_time_list[i]

        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
            utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
        
        await  asyncio.sleep(1)
             
             
async def CoRoutine_BME280():
    """Read temperature, humidity and barometric pressure
       from BME280 sensor
    """
    print('Start CoRoutine_BME280()')
    
    global bme
            
    while True:                
        print('BME280()->', end=' ')
        
        bme = BME280.BME280(i2c=i2c)
        #bme.temperature = bme.temperature -3
        print('Temperature ={} C, Humidity ={} %, Presure ={} hPa, Presure ={} mmHg'.format(
            bme.temperature, bme.humidity, bme.pressure, bme.pressure_mm))
        
        await  asyncio.sleep(10)


async def CoRoutine_Webhooks():
    """
    CoRoutine_Wenhooks()
    google account:  weather.st.home@gmail.com
    password:        Weather_01
    """
    print('Start CoRoutine_Wemhooks()')  
    
    while True:
        print('Webhooks()->', end=' ')
        try:
            #time_list = '{:02d}:{:02d}:{:02d}'.format(utc['hour'], utc['minute'], utc['second'])
            #sensor_readings = {'value1':time_list, 'value2':Temp}
            sensor_readings = {'value1':bme.temperature, 'value2':bme.humidity, 'value3':bme.pressure}
            print(sensor_readings, end=' ')

            request_headers = {'Content-Type': 'application/json'}

            request = urequests.post(
            'http://maker.ifttt.com/trigger/' + event + '/with/key/' + api_key,
            json=sensor_readings,
            headers=request_headers)
            print(request.text)
            request.close()

        except OSError as e:
            print('Failed to read/publish sensor readings.')
            
        await  asyncio.sleep(10)
        
        
#-------------------------------------------------------------------------------#
# Main Loop
#-------------------------------------------------------------------------------#

if __name__ == '__main__':
    
    print('Start Main Loop:')
    
    # read 'index.html'
    page = web_page()
    
    loop = asyncio.get_event_loop()
    loop.create_task ( CoRoutine_Time() )
    loop.create_task ( CoRoutine_BME280() )
    loop.create_task ( CoRoutine_Webhooks() )
    
    app.run(host='0.0.0.0')
    
    utime.sleep(10)
    
    machine.deepsleep(60000)









#-------------------------------------------------------------------------------#
# Don't uise functions
#-------------------------------------------------------------------------------#

def web_page1(data):
    
    html = """<!DOCTYPE HTML><html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="refresh" content="3">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <style>
        html {
         font-family: Arial;
         display: inline-block;
         margin: 0px auto;
         text-align: center;
        }
        h2 { font-size: 2.8rem; }
        h3 { font-size: 1.4rem; vertical-align:middle;}
        p { font-size: 2.8rem; text-align: left;}
        .units { font-size: 1.2rem; }
        .dht-labels{
          font-size: 1.5rem;
          vertical-align:middle;
          padding-bottom: 15px;
        }
      </style>
    </head>
    <body>
      <h2>
        Weather Station
      </h2>
      <h3>
        """+ 'UTC+%d: %d-%02d-%02d %02d:%02d:%02d' % (utc['timezone'], utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']) +"""
      </h3>      
      <p>
        <i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
        <span class="dht-labels">Temperature</span> 
        <span id="temperature">"""+ str(bme.temperature) +"""</span>
        <sup class="units">&deg;C</sup>
      </p>
      <p>
        <i class="fas fa-tint" style="color:#00add6;"></i> 
        <span class="dht-labels">Humidity</span>
        <span id="humidity">"""+ str(bme.humidity) +"""</span>
        <sup class="units">%</sup>
      </p>
      <p>
        <i class="fas fa-angle-double-down" style="color:#00add6;"></i> 
        <span class="dht-labels">Pressure</span>
        <span id="pressure">"""+ str(bme.pressure) +"""</span>
        <sup class="units">hPa</sup>
      </p>
      <p>
        <i class="fas fa-hourglass" style="color:#e8c14d;"></i> 
        <span class="dht-labels">Pressure</span>
        <span id="pressure_mm">"""+ str(bme.pressure_mm) +"""</span>
        <sup class="units">mmHg</sup>
      </p>      
    </body></html>"""
    return html
