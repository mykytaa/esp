#----------------------------------------------------------------------------------------------------#
# boot.py
# Wi-fi manager
#----------------------------------------------------------------------------------------------------#

#import esp
#esp.osdebug(None)

import gc
gc.collect()

import webrepl
webrepl.start()

import wifimgr
from utime import sleep

#----------------------------------------------------------------------------------------------------#
# Wi-Fi Manager
#----------------------------------------------------------------------------------------------------#

wlan = wifimgr.get_connection()
if wlan is None:
    print("Could not initialize the network connection.")
    while True:
        pass  # you shall not pass :D

# Main Code goes here, wlan is a working network.WLAN(STA_IF) instance.
print("ESP OK")
sleep(1)
