# Complete project details at https://RandomNerdTutorials.com

import wifimgr
from time import sleep

try:
    import usocket as socket
except:
    import socket
 
import machine
from machine import Pin, I2C, UART, RTC
import BME280

import utime
import ntptime
import uasyncio as asyncio

# UTC
UTC_TIMEZONE = 3
utc_time_list = ()
utc_year = 0
utc_month = 0
utc_day = 0
utc_hour = 0
utc_minute = 0
utc_second = 0

################################################################################
# Wi-Fi Manager
################################################################################

wlan = wifimgr.get_connection()
if wlan is None:
    print("Could not initialize the network connection.")
    while True:
        pass  # you shall not pass :D

# Main Code goes here, wlan is a working network.WLAN(STA_IF) instance.
print("ESP OK")

################################################################################
# Main
################################################################################

# ESP32 - Pin assignment
i2c = I2C(scl=Pin(19), sda=Pin(23), freq=10000)
sensor_power = Pin(22, Pin.OUT)

# sensor power ON
sensor_power.value(0)

uart = UART(2, 115200)                         # init with given baudrate
uart.init(115200, bits=8, parity=None)         # init with given parameters

bme = BME280.BME280(i2c=i2c)


def web_page():
    """web_page()"""
    
    file = open("index.html", "r")
    page = file.read()
    file.close()        
    return page


async def CoRoutine_Time():
    """CoRoutine_Time()"""
    
    ntptime.settime()
    rtc = machine.RTC()
    
    tm = utime.localtime(utime.mktime(utime.localtime()) + UTC_TIMEZONE*3600)
    tm = tm[0:3] + (0,) + tm[3:6] + (0,)
    rtc.datetime(tm)
    
    while True:
        utc_time_list = utime.localtime(utime.time())
        global utc_year, utc_month, utc_day, utc_hour, utc_minute, utc_second
        utc_year = utc_time_list[0]
        utc_month = utc_time_list[1]
        utc_day = utc_time_list[2]
        utc_hour = utc_time_list[3]
        utc_minute = utc_time_list[4]
        utc_second = utc_time_list[5]
        print('\r\nUTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(UTC_TIMEZONE,
                                                                             utc_time_list[0],
                                                                             utc_time_list[1],
                                                                             utc_time_list[2],
                                                                             utc_time_list[3],
                                                                             utc_time_list[4],
                                                                             utc_time_list[5]))
        
        await  asyncio.sleep(1)   # пауза 1с
             
             
async def CoRoutine_Web():
    """CoRoutine_Web()"""
    
    global UTC_TIMEZONE, utc_year, utc_month, utc_day, utc_hour, utc_minute, utc_second
    
    while True:
        
        #print('Temperature ={} °C, Humidity ={} %, Presure ={} hPa'.format(temperature, humidity, pressure))
        bme = BME280.BME280(i2c=i2c)
        print('Temperature ={} C, Humidity ={} %, Presure ={} hPa, Presure ={} mmHg\r\n'.format(bme.temperature,
                                                                                            bme.humidity,
                                                                                            bme.pressure,
                                                                                            bme.pressure_mm))                
        try:
            if gc.mem_free() < 102000:
                gc.collect()
            conn, addr = s.accept()
            conn.settimeout(3.0)
            print('Got a connection from %s' % str(addr))
            request = conn.recv(1024)
            conn.settimeout(None)
            request = str(request)
            print('Content = %s' % request)
            response = web_page() % (UTC_TIMEZONE, utc_year, utc_month, utc_day, utc_hour, utc_minute, utc_second,
                                     str(bme.temperature), str(bme.humidity), str(bme.pressure), str(bme.pressure_mm))
            conn.send('HTTP/1.1 200 OK\n')
            conn.send('Content-Type: text/html\n')
            conn.send('Connection: close\n\n')
            conn.sendall(response)
            conn.close()
        except OSError as e:
            conn.close()
            print('Connection closed')
        
        await  asyncio.sleep_ms(1)   # пауза 1с


################################################################################
# Main Loop
################################################################################

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('', 80))
    s.listen(5)
except OSError as e:
    machine.reset()

loop = asyncio.get_event_loop()
loop.create_task ( CoRoutine_Time() ) # Запланировать как можно скорее
loop.create_task ( CoRoutine_Web() ) # Запланировать как можно скорее
loop.run_forever()


def web_page1(data):
    
    html = """<!DOCTYPE HTML><html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="refresh" content="3">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <style>
        html {
         font-family: Arial;
         display: inline-block;
         margin: 0px auto;
         text-align: center;
        }
        h2 { font-size: 2.8rem; }
        h3 { font-size: 1.4rem; vertical-align:middle;}
        p { font-size: 2.8rem; text-align: left;}
        .units { font-size: 1.2rem; }
        .dht-labels{
          font-size: 1.5rem;
          vertical-align:middle;
          padding-bottom: 15px;
        }
      </style>
    </head>
    <body>
      <h2>
        Weather Station
      </h2>
      <h3>
        """+ 'UTC+%d: %d-%02d-%02d %02d:%02d:%02d' % (UTC_TIMEZONE, utc_year, utc_month, utc_day, utc_hour, utc_minute, utc_second) +"""
      </h3>      
      <p>
        <i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
        <span class="dht-labels">Temperature</span> 
        <span id="temperature">"""+ str(bme.temperature) +"""</span>
        <sup class="units">&deg;C</sup>
      </p>
      <p>
        <i class="fas fa-tint" style="color:#00add6;"></i> 
        <span class="dht-labels">Humidity</span>
        <span id="humidity">"""+ str(bme.humidity) +"""</span>
        <sup class="units">%</sup>
      </p>
      <p>
        <i class="fas fa-angle-double-down" style="color:#00add6;"></i> 
        <span class="dht-labels">Pressure</span>
        <span id="pressure">"""+ str(bme.pressure) +"""</span>
        <sup class="units">hPa</sup>
      </p>
      <p>
        <i class="fas fa-hourglass" style="color:#e8c14d;"></i> 
        <span class="dht-labels">Pressure</span>
        <span id="pressure_mm">"""+ str(bme.pressure_mm) +"""</span>
        <sup class="units">mmHg</sup>
      </p>      
    </body></html>"""
    return html
