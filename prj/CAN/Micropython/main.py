# Complete project details at https://RandomNerdTutorials.com

try:
    import usocket as socket
except:
    import socket
 
import gc
gc.collect()

from machine import Pin, I2C, UART, CAN
import utime as time
import uasyncio as asyncio

Param1 = 1
Param2 = 2
Param3 = 3
Param4 = 4
Buf1 = 'Hello lunatics'


#################################################################################################################################

def web_page():     
    html = """<html><head><meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="refresh" content="3">
    <link rel="icon" href="data:,"><style>body { text-align: center; font-family: "Trebuchet MS", Arial;}
    table { border-collapse: collapse; width:35%; margin-left:auto; margin-right:auto; }
    th { padding: 12px; background-color: #0043af; color: white; }
    tr { border: 1px solid #ddd; padding: 12px; }
    tr:hover { background-color: #bcbcbc; }
    td { border: none; padding: 12px; }
    .sensor { color:white; font-weight: bold; background-color: #bcbcbc; padding: 1px;
    </style></head><body><h1>ESP w CAN</h1>
    <table><tr><th>MEASUREMENT</th><th>VALUE</th></tr>
    <tr><td>Param1</td><td><span class="sensor">""" + str(Param1) + """</span></td></tr>
    <tr><td>Param2</td><td><span class="sensor">""" + str(Param2)  + """</span></td></tr>
    <tr><td>Param3</td><td><span class="sensor">""" + str(Param3) + """</span></td></tr>
    <tr><td>Param4</td><td><span class="sensor">""" + str(Param4) + """</span></td></tr>
    <tr><td>Buf1</td><td><span class="sensor">""" + Buf1 + """</span></td></tr></body></html>"""
    return html

#################################################################################################################################

async def Web():
    """Web page
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 80))
    s.listen(5)

    while True:    
        print('Param1 ={}, Param2 ={}, Param3 ={}, Param4 ={}, Buf1 ={}'.format(Param1, Param2, Param3, Param4, Buf1))

        try:
            if gc.mem_free() < 102000:
                gc.collect()
            conn, addr = s.accept()
            conn.settimeout(3.0)
            print('Got a connection from %s' % str(addr))
            request = conn.recv(1024)
            conn.settimeout(None)
            request = str(request)
            print('Content = %s' % request)
            response = web_page()
            conn.send('HTTP/1.1 200 OK\n')
            conn.send('Content-Type: text/html\n')
            conn.send('Connection: close\n\n')
            conn.sendall(response)
            conn.close()
        except OSError as e:
            conn.close()
            print('Connection closed')
            
        await  asyncio.sleep(1)   # pause 1 s

#################################################################################################################################
        
async def CAN_Tx():
    """
    CAN-bus Send
    """    
    data = [100,101,102,103,104,105,106,107]
    
    while True :
        # CAN send message
        can.send(data, 0x100)
        # debug print
        print('{:06d} CAN.Tx[0x{:03x}]: {}'.format(time.ticks_ms(), 0x100, data))
          
        await  asyncio.sleep (1)   # pause 1 s
          
#################################################################################################################################
        
async def CAN_Rx():
    """
    CAN-bus Receive
    """        
    buf = ''
    param = list()
    while True:
        
        if can.any() == True:        
            buf = bytearray(8)
            lst = [0, 0, 0, memoryview(buf)]
            # No heap memory is allocated in the following call
            can.recv(lst)
            # debug print            
            #print('{:06d} CAN.Rx[0x{:03x}]: {}'.format(time.ticks_ms(), buf[0], buf[3]))
            print('{:06d} CAN.Rx: {}'.format(time.ticks_ms(), lst))
            print('{:06d} CAN.Rx[0x{:03x}]: {}'.format(time.ticks_ms(), lst[0], lst[3]))
        
        await  asyncio.sleep_ms(1)   # pause 1 s
        
#################################################################################################################################

# GPIO5/GPIO12/GPIO25    CAN TX (D)
# GPIO4/GPIO14/GPIO35    CAN RX (R)
can = CAN(0, tx_io=5, rx_io=4, extframe=False, mode=CAN.NORMAL, baudrate=250, auto_restart=False)

loop  =  asyncio.get_event_loop ()
loop.create_task (CAN_Tx()) #
loop.create_task (CAN_Rx()) #
#loop.create_task (Web())    #
loop.run_forever ()
