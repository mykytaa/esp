
try:
    import usocket as socket
except:
    import socket

import network

#import esp
#esp.osdebug(None)

import gc
gc.collect()

ssid = 'E-line'
password = 'eline111'

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while not station.isconnected():
    pass

print('\r\nConnection successful:', end=' ')
print(station.ifconfig(), end='\r\n')
