#================================================================================#
# main.py
# Web Server based on @PicoWeb
#================================================================================#

import machine
from machine import Pin, I2C, UART, RTC

import utime
import ntptime
import uasyncio as asyncio
import urequests
import ujson
import picoweb
from ucollections import OrderedDict
import utelegram
import bmp280 


#-------------------------------------------------------------------------------#
# Constants
#-------------------------------------------------------------------------------#

# Telegram bot
API_KEY = '1250677371:AAFWqWxbl3U24BC3hQWjyzES85mJ0z8-1Hs'
CHAT_ID = 478650453

# Bmp280 Sensor
mmHg =0.0075006375541921

#-------------------------------------------------------------------------------#
# Init Peripherals
#-------------------------------------------------------------------------------#

# ESP32 - Pin assignment

# USART
#uart = UART(1, tx=33, rx=25)
#uart = UART(1, tx=14, rx=12)
uart = UART(1, tx=5, rx=18)
uart.init(115200, bits=8, parity=None, timeout=10)
#uart.init(921600, bits=8, parity=None, timeout=10)
uart_power = Pin(17, Pin.OUT)
uart_power.value(0)

# Pin LED
led = Pin(22, Pin.OUT)
led.value(1)

# I2C
i2c = I2C(scl=Pin(2), sda=Pin(0), freq=10000)

# BMP280
bmp_vcc = Pin(13, Pin.OUT)
bmp_gnd = Pin(15, Pin.OUT)
bmp_vcc.value(1) #off
bmp_gnd.value(0) #gnd
bmp = bmp280.BMP280(i2c)

# Web
app = picoweb.WebApp(__name__)

# Telegram bot
bot = utelegram.ubot(API_KEY)


#-------------------------------------------------------------------------------#
# Gloabal Variables
#-------------------------------------------------------------------------------#

# UTC
utc_time_list = (0,0,0,0,0,0)
utc = {'timezone': +3, 'year': 0, 'month': 0, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0}
# JSON
Module_Status = OrderedDict(
{
    'IoCore': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Dpm': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Ccs': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Chademo': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Gbt': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'J1772': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Schuko': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    },
    'Meter': {
        'Stat': 0,
        'Timeout': 10000,
        'Time': 0
    }
})

# Module_Parameter = {
#     'IOCore': {'Ver':'0.0'}, 'Dpm': {'Ver':'0.0'}, 'Ccs': {'Ver':'0.0'}, 'Chademo': {'Ver':'0.0'}, 'Gbt': {'Ver':'0.0'},
#     'J1772': {'Ver':'0.0'}, 'Schuko': {'Ver':'0.0'},'Meter': {'Ver':'0.0'}
# }

Module_Parameter = {
    'Combo': 0, 'IoCore': 0, 'Dpm': 0, 'Ccs': 0, 'Chademo': 0, 'Gbt': 0, 'J1772': 0, 'Schuko': 0,'Meter': 0
}

Resp = ''

Combo_connect =0
Dpm_reboot =0
Dpm_blk_n =0
Dpm_switch =0
Dpm_relay_ccs=0
Dpm_relay_cha=0
Dpm_relay_gbt=0

Ccs_reboot =0
Ccs_start =0

Chademo_reboot =0
Chademo_start =0

IoCore_switch =0
IoCore_relay =0

Message = {
    "update_id":302445393,
    "message":{
        "message_id":1492,
        "from":{
            "id":123456789,
            "is_bot":False,
            "language_code":"en",
            "first_name":"Mykyta"
        },
        "text":"/ping",
        "date":1599563930,
        "entities":[
        {
            "offset":0,
            "length":5,
            "type":"bot_command"
        }
        ],
        "chat":{
        "id":478650453,
        "type":"private",
        "first_name":"Mykyta"
        }
    }
}


#-------------------------------------------------------------------------------#
# Functions
#-------------------------------------------------------------------------------#

def TimeSync():
    """Time sync from www
    """
    #print('Start Func_TimeSync()')

    try:
        print('ntptime sync->', end=' ')

        ntptime.settime()
        tm = utime.localtime(utime.mktime(utime.localtime()) + utc['timezone']*3600)
        RTC().datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]
        print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
                utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))
        return True
    except Exception:
        print('Error.')
        return False


#-------------------------------------------------------------------------------#

def TimeGet():
    """Time get from rtc
    """
    #print('Start Func_TimeGet()')

    tm = utime.localtime(utime.time())

    utc['year']   = tm[0]
    utc['month']  = tm[1]
    utc['day']    = tm[2]
    utc['hour']   = tm[3]
    utc['minute'] = tm[4]
    utc['second'] = tm[5]

    #print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
    #utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))

    return ('{:02d}:{:02d}:{:02d}'.format(utc['hour'], utc['minute'], utc['second']))


#-------------------------------------------------------------------------------#

def VerifyModulesConnect():
    """
    """
    #print('Start JsonPars()')

    for module in Module_Status:

        if utime.ticks_diff(utime.ticks_ms(), Module_Status[module]['Time']) > Module_Status[module]['Timeout']:

            Module_Status[module]['Stat'] = 0
            Module_Parameter[module] = {'Ver':'0.0'}


#-------------------------------------------------------------------------------#

def JsonPars(data):
    """JSON passed object
    """

    global Resp

    #print('Start JsonPars()')

    try:
        print('%s JsonRx:' % TimeGet(), end=' ')
        ##print(data)
        for module in data:
            for module1 in Module_Parameter:
                if(module == module1):
                    # last time received msg and stat online
                    if(module !='Combo'):
                        Module_Status[module]['Time'] = utime.ticks_ms()
                        Module_Status[module]['Stat'] = 1
                    # Variant1: rewrite new dict
                    Module_Parameter[module] = data[module]
                    print('%s:' % (module), end=' ')
                    for key in data[module]:
                        # Variant1: add new dict
                        #Module_Parameter[module][key] = data[module][key]
                        print('%s=%s,' % (key, Module_Parameter[module][key]), end=' ')
        print('\r')
    except Exception:
        print('ex error')

    ###########
    # Handler #
    ###########

    if('Resp' in (Module_Parameter['Combo'].keys())):
        Resp =Module_Parameter['Combo']['Resp']
        Module_Parameter['Combo'] = 0

    if('VAmax' in (Module_Parameter['Dpm'].values())):
        buf1, buf2 = Module_Parameter['Dpm']['VAmax'].split('@',2)
        Imax = int(float(buf2))
        buf1, buf2 = Module_Parameter['Dpm']['blkVA'].split('@',2)
        buf1, buf3 = buf2.split('-',2)
        blkImax = int(buf3)
        Dpm_blk_n = Imax/blkImax
        #print(Imax, blkImax, Dpm_blk_n)


#===============================================================================#
# async func #
#===============================================================================#

async def aFunc_Time(mon):
    """Read Data & Time from www
    """
    print('Start Func_Time()')

    TimeSync()

    while(True):

        #print('Time()->', end=' ')

        tm = utime.localtime(utime.time())
        utc['year']   = tm[0]
        utc['month']  = tm[1]
        utc['day']    = tm[2]
        utc['hour']   = tm[3]
        utc['minute'] = tm[4]
        utc['second'] = tm[5]

        VerifyModulesConnect()

        if(mon == 1):
            print('UTC+{:d}: {:4d}.{:02d}.{:02d} {:02d}:{:02d}:{:02d}'.format(utc['timezone'],
                utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']))

        await  asyncio.sleep(1)


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

async def aFunc_Uart_Tx():
    """Send time % weather to uart.
    """
    print('Start Func_Uart()')

    while(True):

        print('%s Uart_Tx: ' % TimeGet() + 'Sync msg')

        VerifyModulesConnect()

        await  asyncio.sleep(10)


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

async def aFunc_Uart_Rx():
    """Receive data line (ended='\n') from uart.
    """
    print('Start aFunc_Uart_Rx()')
    #uart.write('Start aFunc_Uart_Rx1()')

    sreader = asyncio.StreamReader(uart)
    while True:
        res = await sreader.readline()
        print('%s UartRx:' % TimeGet(), end=' ')
        print(res)
        try:
            js = ujson.loads(res)
            ##print(js)
            JsonPars(js)
        except Exception:
            print('json err')

        #await  asyncio.sleep_ms (1)


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#

def get_message(message):
    bot.send(message['message']['chat']['id'], message['message']['text'].upper())    

def reply_ping(message):
    #print(message)
    #buf = 'T=%.1f C, P=%d hP' % (23.4, 1001)
    bot.send(message['message']['chat']['id'], 'pong')
    #print(buf)
        
async def aFunc_TelegaRxResp():
    """.
    """
    print('Start aFunc_TelegaRxResp()')
    
    bot.register('/ping', reply_ping)
    bot.set_default_handler(get_message)
        
    while(True):
        bot.read_once()
        await  asyncio.sleep(1)

async def aFunc_TelegaTx():
    """.
    """
    print('Start aFunc_TelegaTx()')
    
    bmp.use_case(bmp280.BMP280_CASE_WEATHER)
    bmp.oversample(bmp280.BMP280_OS_HIGH)

    bmp.temp_os = bmp280.BMP280_TEMP_OS_8
    bmp.press_os = bmp280.BMP280_PRES_OS_4

    bmp.standby = bmp280.BMP280_STANDBY_250
    bmp.iir = bmp280.BMP280_IIR_FILTER_2
    
    bmp.normal_measure()
       
    while(True):
        
        await  asyncio.sleep(1)
        
        buf = '%s\r\nt=%.1f %s, p=%d hPa (%d mmHg)' % (TimeGet(), bmp.temperature, '°C', round(bmp.pressure/100), bmp.pressure*mmHg)
        bot.send(CHAT_ID, buf)
        print(buf)
        await  asyncio.sleep(600)
        
#===============================================================================#
# web pages #
#===============================================================================#

def page_index():
    """Index page
    """
    # Calculate online modules in system
    present=[]
    for module in Module_Status:
        if Module_Status[module]['Stat'] == 1:
            present.append(module)

    # autorefresh: <meta http-equiv="refresh" content="3">

    page = """
    <html><head><title>Combo</title><style>
        h1{font-size: 80px;}
        h2{font-size: 40px;}
        p{font-size: 40px;}
        table, th, td {font-size: 50px;}
        .button {background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px;}
        .button2 {background-color:#4286f4;}
        .button3 {font-size: 30px;}
        </style></head>
      <body>
        <h1>Combo</h1>
        <h2>"""+ 'UTC+%d: %d-%02d-%02d %02d:%02d:%02d' % (utc['timezone'], utc['year'], utc['month'], utc['day'], utc['hour'], utc['minute'], utc['second']) +"""</h2>
        <p>Connect: """+'%s' % (present) +"""</p>
        <p>DC: <a href="/IoCore"><button class="button">IOCore</button></a>
               <a href="/Dpm"><button class="button">DPM</button></a>
               <a href="/Ccs"><button class="button">CCS</button></a>
               <a href="/Chademo"><button class="button">CHAdeMO</button></a>
               <a href="/Gbt"><button class="button">GB/t</button></a></p>
        <p>AC: <a href="/J1772"><button class="button button2">J1772</button></a>
               <a href="/Schuko"><button class="button button2">Schuko</button></a>
               <a href="/Meter"><button class="button button2">Meter</button></a></p>
        <form action="form" name="form">
        <p>Combo:
        <button class="button3" name="combo_connect" value="1">Connect</button>
        <button class="button3" name="combo_connect" value="0">Disconnect</button>
        </p></form>
      </body>
    </html>"""
    return page

#-------------------------------------------------------------------------------#

# Index page
@app.route('/')
def index(req, resp):
    # Start HTTP response with content-type text/html
    yield from picoweb.start_response(resp)
    # Send actual HTML page
    yield from resp.awrite(page_index())


#===============================================================================#

@app.route("/form")
def form(req, resp):
    # GET, apparently
    # Note: parse_qs() is not a coroutine, but a normal function.
    # But you can call it using yield from too.
    req.parse_qs()

    # Whether form data comes from GET or POST request, once parsed,
    # it's available as req.form dictionary

    global IoCore_switch, IoCore_relay
    global Dpm_switch, Dpm_relay_ccs, Dpm_relay_cha, Dpm_relay_gbt, Dpm_blk_n
    global Ccs_start, Chademo_start
    global Resp

    print('%s: %s=' % (TimeGet(), req.method), end=' ')
    print(req.form)

    page = 'Error'
    buf = ''

    for key in req.form:

        if(key =='combo_connect'):
            Combo_connect = int(req.form[key])
            # JSON
            buf = '{"Combo":{"Sync":"%d"}}\r\n' % (Combo_connect)
            page=page_index()

        elif(key =='iocore_switch'):
            IoCore_switch = int(req.form[key])
            # JSON
            buf = '{"IoCore":{"Sw":"%d"}}\r\n' % (IoCore_switch)
            page=page_iocore_ctrl()

        elif(key =='iocore_relay'):
            IoCore_relay = int(req.form[key])
            # JSON
            buf = '{"IoCore":{"Rel":"%d"}}\r\n' % (IoCore_relay)
            page=page_iocore_ctrl()

        elif(key =='iocore_reboot'):
            IoCore_reboot = int(req.form[key])
            # JSON
            if(IoCore_reboot):
                buf = '{"IoCore":{"Reboot":"%d"}}\r\n' % (IoCore_reboot)
                page=page_iocore_conf()

        elif(key =='dpm_switch'):
            Dpm_switch = int(req.form[key])
            led.value(not Dpm_switch)
            # JSON
            buf = '{"Dpm":{"Sw":"%d"}}\r\n' % (Dpm_switch)
            page=page_dpm_ctrl()

        elif(key =='dpm_relay1'):
            Dpm_relay_ccs = int(req.form[key])
            # JSON
            buf = '{"Dpm":{"Rel1":"%d"}}\r\n' % ( (1<<8)|(Dpm_relay_ccs) )
            page=page_dpm_ctrl()

        elif(key =='dpm_relay2'):
            Dpm_relay_cha = int(req.form[key])
            # JSON
            buf = '{"Dpm":{"Rel2":"%d"}}\r\n' % ( (2<<8)|(Dpm_relay_cha) )
            page=page_dpm_ctrl()

        elif(key =='dpm_relay3'):
            Dpm_relay_gbt = int(req.form[key])
            # JSON
            buf = '{"Dpm":{"Rel3":"%d"}}\r\n' % ( (3<<8)|(Dpm_relay_gbt) )
            page=page_dpm_ctrl()

        elif(key =='dpm_reboot'):
            Dpm_reboot = int(req.form[key])
            # JSON
            if(Dpm_reboot):
                buf = '{"Dpm":{"Reboot":"%d"}}\r\n' % Dpm_reboot
                page=page_dpm_conf()

        elif(key =='dpm_number'):
            Dpm_blk_n = int(req.form[key])
            # JSON
            buf = '{"Dpm":{"Blk_n":"%d"}}\r\n' % (Dpm_blk_n)
            page=page_dpm_conf()

        elif(key =='ccs_reboot'):
            Ccs_reboot = int(req.form[key])
            # JSON
            if(Ccs_reboot):
                buf = '{"Ccs":{"Reboot":"%d"}}\r\n' % Ccs_reboot
                page=page_ccs_conf()

        elif(key =='ccs_start'):
            Ccs_start = int(req.form[key])
            # JSON
            buf = '{"Ccs":{"Start":"%d"}}\r\n' % (Ccs_start)
            page=page_ccs_ctrl()

        elif(key =='chademo_reboot'):
            Chademo_reboot = int(req.form[key])
            # JSON
            if(Chademo_reboot):
                buf = '{"Chademo":{"Reboot":"%d"}}\r\n' % Chademo_reboot
                page=page_chademo_conf()

        elif(key =='chademo_start'):
            Chademo_start = int(req.form[key])
            # JSON
            buf = '{"Chademo":{"Start":"%d"}}\r\n' % (Chademo_start)
            page=page_chademo_ctrl()

        yield from picoweb.start_response(resp)

        if(buf != ''):
            Resp = 'Timeout'
            uart.write(buf)
            #await asyncio.sleep(1)
            print('%s UartTx: %s' % (TimeGet(), buf), end='')
            await asyncio.sleep(1)
            yield from resp.awrite(
                '<script>alert("Response: %s");</script>' % Resp
            )
            await asyncio.sleep(2)

        # Send actual HTML page
        yield from resp.awrite(page)


#===============================================================================#

# Another one, more complicated page
@app.route('/IoCore')
def iocore(req, resp):
        # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo IOCore</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>IOCore</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["IoCore"] !=0):
        yield from resp.awrite(
            '<table border=1>')
        for key in Module_Parameter["IoCore"]:
            yield from resp.awrite(
                '<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["IoCore"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/IoCore_conf"><button class="button button2">Config</button></a>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')

#-------------------------------------------------------------------------------#

# def page_iocore_ctrl():
#     """IOCore control page
#     button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
#     center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
#     input {border-radius: 100px;width: 60px;height: 60px;}
#     """
#
#     if(IoCore_switch):
#         switch_checked = 'checked'
#     else: switch_checked = ''
#     if(IoCore_relay):
#         relay_checked = 'checked'
#     else: relay_checked = ''
#
#     page = """
#     <html><head><title>IOCore Control</title>
#         <style>
#             h1{font-size: 80px;}
#             h2{font-size: 40px;}
#             h3{font-size: 30px;}
#             p{font-size: 40px;}
#             input[type=radio] {width: 60px;height: 60px;}
#             input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
#             .switch{position:relative;display:inline-block;width:120px;height:68px}.switch input{display:none}
#             .slider{position:absolute;top:0;left:0;right:0;bottom:0;background-color:#ccc;border-radius:34px}
#             .slider:before{position:absolute;content:"";height:52px;width:52px;left:8px;bottom:8px;background-color:#fff;-webkit-transition:.4s;transition:.4s;border-radius:68px}
#             input:checked+.slider{background-color:#2196F3}
#             input:checked+.slider:before{-webkit-transform:translateX(52px);-ms-transform:translateX(52px);transform:translateX(52px)}
#         </style></head>
#         <script>
#             function toggleCheckbox(element) {
#                 var xhr = new XMLHttpRequest();
#                 if(element.checked) {
#                     if(element.id=="iocore_switch") {
#                         xhr.open("GET", "/form?iocore_switch=1", true);
#                     }
#                     else if(element.id=="iocore_relay") {
#                         xhr.open("GET", "/form?iocore_relay=1", true);
#                     }
#                 }
#                 else {
#                     if(element.id=="iocore_switch") {
#                         xhr.open("GET", "/form?iocore_switch=0", true);
#                     }
#                     else if(element.id=="iocore_relay") {
#                         xhr.open("GET", "/form?iocore_relay=0", true);
#                     }
#                 }
#                 xhr.send();
#             }
#         </script>
#         <body><h1>IOCore Control</h1>
#         <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
#         <h3>Switch</h3>
#         <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="iocore_switch" %s><span class="slider"></span></label>
#         <h3>Relay</h3>
#         <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="iocore_relay" %s><span class="slider"></span></label>
#         <p><a href="/IoCore">back</a></p>
#     </html>
#     """ % (switch_checked, relay_checked)
#     return page

#-------------------------------------------------------------------------------#

def page_iocore_conf():
    """IOCore configuration page
    button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
    center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
    input {border-radius: 100px;width: 60px;height: 60px;}
    """

    reboot_checked_0 = 'checked'
    reboot_checked_1 = ''

    page = """
    <html><head><title>IOCore Config</title>
        <style>
            h1{font-size: 80px;}
            h2{font-size: 40px;}
            h3{font-size: 30px;}
            p{font-size: 40px;}
            input[type=radio] {width: 60px;height: 60px;}
            input[type=text] {width: 60px;height: 60px; font-size: 30px;}
            input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
        </style></head>
        <body><h1>IOCore Config</h1>
        <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
        <h3>Reboot:
        <form action="form" name="form1" method "post">
        <input type="radio" name="iocore_reboot" value="0" """+'%s' % reboot_checked_0+""">
        <input type="radio" name="iocore_reboot" value="1" """+'%s' % reboot_checked_1+""">reboot
        <input type="submit" value="Submit"></form></h3>
        <p><a href="/IoCore">back</a></p>
    </html>"""
    return page

#-------------------------------------------------------------------------------#

# @app.route('/IoCore_ctrl')
# def conf(req, resp):
#     # Start HTTP response with content-type text/html
#
#     yield from picoweb.start_response(resp)
#     yield from resp.awrite(page_iocore_ctrl())

#-------------------------------------------------------------------------------#

@app.route('/IoCore_conf')
def iocore_conf(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(page_iocore_conf())


#===============================================================================#

@app.route('/Dpm')
def dpm(req, resp):
    # Start HTTP response with content-type text/html

    #await response.start_html()
    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo DPM</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>DPM</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Dpm"] !=0):
        yield from resp.awrite(
            '<table border=1>')
        for key in Module_Parameter["Dpm"]:
            yield from resp.awrite(
                '<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Dpm"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/Dpm_ctrl"><button class="button">Control</button></a>'
            ' <a href="/Dpm_conf"><button class="button button2">Config</button></a>'
            '</p>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')

#-------------------------------------------------------------------------------#

def page_dpm_ctrl():
    """Dpm control page
    button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
    center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
    input {border-radius: 100px;width: 60px;height: 60px;}
    """

    if(Dpm_switch):
        switch_checked = 'checked'
    else: switch_checked = ''

    if(Dpm_relay_ccs):
        relay1_checked = 'checked'
    else: relay1_checked = ''

    if(Dpm_relay_cha):
        relay2_checked = 'checked'
    else: relay2_checked = ''

    if(Dpm_relay_gbt):
        relay3_checked = 'checked'
    else: relay3_checked = ''

    page = """
    <html><head><title>DPM Control</title>
        <style>
            h1{font-size: 80px;}
            h2{font-size: 40px;}
            h3{font-size: 30px;}
            p{font-size: 40px;}
            input[type=radio] {width: 60px;height: 60px;}
            input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
            .switch{position:relative;display:inline-block;width:120px;height:68px}
            .switch input{display:none}
            .slider{position:absolute;top:0;left:0;right:0;bottom:0;background-color:#ccc;border-radius:34px}
            .slider:before{position:absolute;content:"";height:52px;width:52px;left:8px;bottom:8px;background-color:#fff;-webkit-transition:.4s;transition:.4s;border-radius:68px}
            input:checked+.slider{background-color:#2196F3}
            input:checked+.slider:before{-webkit-transform:translateX(52px);-ms-transform:translateX(52px);transform:translateX(52px)}

            .slider2 { -webkit-appearance: none; margin: 14px; width: 300px; height: 20px; background: #ccc; outline: none; -webkit-transition: .2s; transition: opacity .2s;}
            .slider2::-webkit-slider-thumb {-webkit-appearance: none; appearance: none; width: 30px; height: 30px; background: #2f4468; cursor: pointer;}
            .slider2::-moz-range-thumb { width: 30px; height: 30px; background: #2f4468; cursor: pointer; }
        </style></head>
        <script>
        function toggleCheckbox(element) {
            var xhr = new XMLHttpRequest();
            if(element.checked) {
                if(element.id=="dpm_switch") {
                    xhr.open("GET", "/form?dpm_switch=1", true);
                }
                else if(element.id=="dpm_relay1") {
                    xhr.open("GET", "/form?dpm_relay1=1", true);
                }
                else if(element.id=="dpm_relay2") {
                    xhr.open("GET", "/form?dpm_relay2=1", true);
                }
                else if(element.id=="dpm_relay3") {
                    xhr.open("GET", "/form?dpm_relay3=1", true);
                }
            }
            else {
                if(element.id=="dpm_switch") {
                    xhr.open("GET", "/form?dpm_switch=0", true);
                }
                else if(element.id=="dpm_relay1") {
                    xhr.open("GET", "/form?dpm_relay1=0", true);
                }
                else if(element.id=="dpm_relay2") {
                    xhr.open("GET", "/form?dpm_relay2=0", true);
                }
                else if(element.id=="dpm_relay3") {
                    xhr.open("GET", "/form?dpm_relay3=0", true);
                }
            }
            xhr.send();
        }
        </script>
        <body><h1>DPM Control</h1>
        <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
        <h3>Switch</h3>
        <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="dpm_switch" %s><span class="slider"></span></label>
        <h3>Relay ccs</h3>
        <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="dpm_relay1" %s><span class="slider"></span></label>
        <h3>Relay chademo</h3>
        <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="dpm_relay2" %s><span class="slider"></span></label>
        <h3>Relay gbt</h3>
        <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="dpm_relay3" %s><span class="slider"></span></label>
        <p><a href="/Dpm">back</a></p>
    </html>
    """ % (switch_checked, relay1_checked, relay2_checked, relay3_checked)
    return page

#-------------------------------------------------------------------------------#

def page_dpm_conf():
    """Dpm configuration page
    button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
    center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
    input {border-radius: 100px;width: 60px;height: 60px;}
    """

    number_checked1 = ''
    number_checked2 = ''
    number_checked3 = ''
    number_checked4 = ''

    reboot_checked_0 = 'checked'
    reboot_checked_1 = ''

    if(Dpm_blk_n == 1):
        number_checked1 = 'checked'
    elif(Dpm_blk_n == 2):
        number_checked2 = 'checked'
    elif(Dpm_blk_n == 3):
        number_checked3 = 'checked'
    elif(Dpm_blk_n == 4):
        number_checked4 = 'checked'

    page = """
    <html><head><title>DPM Config</title>
        <style>
            h1{font-size: 80px;}
            h2{font-size: 40px;}
            h3{font-size: 30px;}
            p{font-size: 40px;}
            input[type=radio] {width: 60px;height: 60px;}
            input[type=text] {width: 60px;height: 60px; font-size: 30px;}
            input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
        </style></head>
        <body><h1>DPM Config</h1>
        <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
        <h3>Blk Number:
        <form action="form" name="form" method "post">
        <input type="radio" name="dpm_number" value="1" """+'%s' % number_checked1+""">1
        <input type="radio" name="dpm_number" value="2" """+'%s' % number_checked2+""">2
        <input type="radio" name="dpm_number" value="3" """+'%s' % number_checked3+""">3
        <input type="radio" name="dpm_number" value="4" """+'%s' % number_checked4+""">4
        <input type="submit" value="Submit"></form></h3>
        <h3>Reboot:
        <form action="form" name="form1" method "post">
        <input type="radio" name="dpm_reboot" value="0" """+'%s' % reboot_checked_0+""">
        <input type="radio" name="dpm_reboot" value="1" """+'%s' % reboot_checked_1+""">reboot
        <input type="submit" value="Submit"></form></h3>
        <p><a href="/Dpm">back</a></p>
    </html>"""
    return page

#-------------------------------------------------------------------------------#

@app.route('/Dpm_ctrl')
def dpm_ctrl(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(page_dpm_ctrl())

#-------------------------------------------------------------------------------#

@app.route('/Dpm_conf')
def dpm_conf(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(page_dpm_conf())


#===============================================================================#

@app.route('/Ccs')
def ccs(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo CCS</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>CCS</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Ccs"] !=0):
        yield from resp.awrite('<table border=1>')
        for key in Module_Parameter["Ccs"]:
            yield from resp.awrite('<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Ccs"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/Ccs_ctrl"><button class="button">Control</button></a>'
            ' <a href="/Ccs_conf"><button class="button button2">Config</button></a>'
            '</p>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')

#-------------------------------------------------------------------------------#

def page_ccs_ctrl():
    """CCS control page
    button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
    center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
    input {border-radius: 100px;width: 60px;height: 60px;}
    """

    if(Ccs_start):
        start_checked = 'checked'
    else: start_checked = ''

    page = """
    <html><head><title>CCS Control</title>
        <style>
            h1{font-size: 80px;}
            h2{font-size: 40px;}
            h3{font-size: 30px;}
            p{font-size: 40px;}
            input[type=radio] {width: 60px;height: 60px;}
            input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
            .switch{position:relative;display:inline-block;width:120px;height:68px}.switch input{display:none}
            .slider{position:absolute;top:0;left:0;right:0;bottom:0;background-color:#ccc;border-radius:34px}
            .slider:before{position:absolute;content:"";height:52px;width:52px;left:8px;bottom:8px;background-color:#fff;-webkit-transition:.4s;transition:.4s;border-radius:68px}
            input:checked+.slider{background-color:#2196F3}
            input:checked+.slider:before{-webkit-transform:translateX(52px);-ms-transform:translateX(52px);transform:translateX(52px)}
        </style></head>
        <script>
            function toggleCheckbox(element) {
                var xhr = new XMLHttpRequest();
                if(element.checked) {
                    if(element.id=="ccs_start") {
                        xhr.open("GET", "/form?ccs_start=1", true);
                    }
                }
                else {
                    if(element.id=="ccs_start") {
                        xhr.open("GET", "/form?ccs_start=0", true);
                    }
                }
                xhr.send();
            }
        </script>
        <body><h1>CCS Control</h1>
        <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
        <h3>Start</h3>
        <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="ccs_start" %s><span class="slider"></span></label>
        <p><a href="/Ccs">back</a></p>
    </html>
    """ % (start_checked)
    return page

#-------------------------------------------------------------------------------#

def page_ccs_conf():
    """CCS Configuration page
    button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
    center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
    input {border-radius: 100px;width: 60px;height: 60px;}
    """

    reboot_checked_0 = 'checked'
    reboot_checked_1 = ''

    page = """
    <html><head><title>CCS Config</title>
        <style>
            h1{font-size: 80px;}
            h2{font-size: 40px;}
            h3{font-size: 30px;}
            p{font-size: 40px;}
            input[type=radio] {width: 60px;height: 60px;}
            input[type=text] {width: 60px;height: 60px; font-size: 30px;}
            input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
        </style></head>
        <body><h1>CCS Config</h1>
        <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
        <h3>Reboot:
        <form action="form" name="form">
        <input type="radio" name="ccs_reboot" value="0" """+'%s' % reboot_checked_0+""">
        <input type="radio" name="ccs_reboot" value="1" """+'%s' % reboot_checked_1+""">reboot
        <input type="submit" value="Submit"></form></h3>
        <p><a href="/Ccs">back</a></p>
    </html>"""
    return page

#-------------------------------------------------------------------------------#

@app.route('/Ccs_ctrl')
def ccs_ctrl(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(page_ccs_ctrl())

#-------------------------------------------------------------------------------#

@app.route('/Ccs_conf')
def ccs_conf(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(page_ccs_conf())


#===============================================================================#

@app.route('/Chademo')
def chademo(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo CHAdeMO</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>CHAdeMO</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Chademo"] !=0):
        yield from resp.awrite('<table border=1>')
        for key in Module_Parameter["Chademo"]:
            yield from resp.awrite(
                '<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Chademo"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/Chademo_ctrl"><button class="button">Control</button></a>'
            ' <a href="/Chademo_conf"><button class="button button2">Config</button></a>'
            '</p>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')

#-------------------------------------------------------------------------------#

def page_chademo_ctrl():
    """Chademo Control page
    button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
    center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
    input {border-radius: 100px;width: 60px;height: 60px;}
    """

    if(Chademo_start):
        start_checked = 'checked'
    else: start_checked = ''

    page = """
    <html><head><title>Chademo Control</title>
        <style>
            h1{font-size: 80px;}
            h2{font-size: 40px;}
            h3{font-size: 30px;}
            p{font-size: 40px;}
            input[type=radio] {width: 60px;height: 60px;}
            input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
            .switch{position:relative;display:inline-block;width:120px;height:68px}.switch input{display:none}
            .slider{position:absolute;top:0;left:0;right:0;bottom:0;background-color:#ccc;border-radius:34px}
            .slider:before{position:absolute;content:"";height:52px;width:52px;left:8px;bottom:8px;background-color:#fff;-webkit-transition:.4s;transition:.4s;border-radius:68px}
            input:checked+.slider{background-color:#2196F3}
            input:checked+.slider:before{-webkit-transform:translateX(52px);-ms-transform:translateX(52px);transform:translateX(52px)}
        </style></head>
        <script>
            function toggleCheckbox(element) {
                var xhr = new XMLHttpRequest();
                if(element.checked) {
                    if(element.id=="chademo_start") {
                        xhr.open("GET", "/form?chademo_start=1", true);
                    }
                }
                else {
                    if(element.id=="chademo_start") {
                        xhr.open("GET", "/form?chademo_start=0", true);
                    }
                }
                xhr.send();
            }
        </script>
        <body><h1>Chademo Control</h1>
        <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
        <h3>Start</h3>
        <label class="switch"><input type="checkbox" onchange="toggleCheckbox(this)" id="chademo_start" %s><span class="slider"></span></label>
        <p><a href="/Chademo">back</a></p>
    </html>
    """ % (start_checked)
    return page

#-------------------------------------------------------------------------------#

def page_chademo_conf():
    """Chademo Configuration page
    button:  <button name="led" value="1">Led On </button> <button name="led" value="0">Led Off</button>
    center:  body{font-family:Arial; text-align: center; margin: 0px auto; padding-top:30px;}
    input {border-radius: 100px;width: 60px;height: 60px;}
    """

    reboot_checked_0 = 'checked'
    reboot_checked_1 = ''

    page = """
    <html><head><title>Chademo Config</title>
        <style>
            h1{font-size: 80px;}
            h2{font-size: 40px;}
            h3{font-size: 30px;}
            p{font-size: 40px;}
            input[type=radio] {width: 60px;height: 60px;}
            input[type=text] {width: 60px;height: 60px; font-size: 30px;}
            input[type=submit] {width: 120px;height: 60px; font-size: 30px;}
        </style></head>
        <body><h1>Chademo Config</h1>
        <h2>Time: """+'%02d:%02d:%02d' % (utc['hour'], utc['minute'], utc['second'])+"""</h2>
        <h3>Reboot:
        <form action="form" name="form">
        <input type="radio" name="chademo_reboot" value="0" """+'%s' % reboot_checked_0+""">
        <input type="radio" name="chademo_reboot" value="1" """+'%s' % reboot_checked_1+""">reboot
        <input type="submit" value="Submit"></form></h3>
        <p><a href="/Chademo">back</a></p>
    </html>"""
    return page

#-------------------------------------------------------------------------------#

@app.route('/Chademo_ctrl')
def conf(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(page_chademo_ctrl())

#-------------------------------------------------------------------------------#

@app.route('/Chademo_conf')
def conf(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(page_chademo_conf())


#===============================================================================#

@app.route('/Gbt')
def gbt(req, resp):
    # Start HTTP response with content-type text/html

    #await response.start_html()
    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo GB/t</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>GB/t</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Gbt"] !=0):
        yield from resp.awrite(
            '<table border=1>')
        for key in Module_Parameter["Gbt"]:
            yield from resp.awrite(
                '<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Gbt"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/Gbt_ctrl"><button class="button">Control</button></a>'
            ' <a href="/Gbt_conf"><button class="button button2">Config</button></a>'
            '</p>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')


#===============================================================================#

@app.route('/J1772')
def j1772(req, resp):
    # Start HTTP response with content-type text/html

    #await response.start_html()
    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo J1772</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>J1772</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["J1772"] !=0):
        yield from resp.awrite(
            '<table border=1>')
        for key in Module_Parameter["J1772"]:
            yield from resp.awrite(
                '<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["J1772"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/J1772_ctrl"><button class="button">Control</button></a>'
            ' <a href="/J1772_conf"><button class="button button2">Config</button></a>'
            '</p>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')


#===============================================================================#

@app.route('/Schuko')
def schuko(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo Schuko</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{ font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>Schuko</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Schuko"] !=0):
        yield from resp.awrite(
            '<table border=1>')
        for key in Module_Parameter["Schuko"]:
            yield from resp.awrite(
                '<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Schuko"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/Schuko_ctrl"><button class="button">Control</button></a>'
            ' <a href="/Schuko_conf"><button class="button button2">Config</button></a>'
            '</p>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')


#===============================================================================#

@app.route('/Meter')
def meter(req, resp):
    # Start HTTP response with content-type text/html

    yield from picoweb.start_response(resp)
    yield from resp.awrite(
        '<html><head><title>Combo Meter display</title><style>'
        'h1{font-size: 80px;}'
        'h2{font-size: 40px;}'
        'p{font-size: 40px;}'
        'table, th, td {font-size: 50px;}'
        '.button { background-color: #e7bd3b; border: 1; border-radius: 4px; font-size: 40px; }'
        '.button2 {background-color:#4286f4;}'
        '</style></head>')
    yield from resp.awrite(
        '<body><h1>Meter</h1>'
        '<h2>Time: %02d:%02d:%02d</h2>' % (utc['hour'], utc['minute'], utc['second']))
    if(Module_Parameter["Meter"] !=0):
        yield from resp.awrite(
            '<table border=1>')
        for key in Module_Parameter["Meter"]:
            yield from resp.awrite(
                '<tr><td>{}</td><td>{}</td></tr>'.format(key, Module_Parameter["Meter"][key]))
        yield from resp.awrite(
            '</table>'
            '<p><a href="/Meter_ctrl"><button class="button">Control</button></a>'
            ' <a href="/Meter_conf"><button class="button button2">Config</button></a>'
            '</p>')
    else:
        yield from resp.awrite(
            '<p>no connect</p>')
    yield from resp.awrite(
        '<p><a href="/">back</a></p>'
        '</html>')


#===============================================================================#
# Main Loop
#===============================================================================#

if __name__ == '__main__':
    print('Start Main:')

    import ulogging as logging
    #logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    #TimeSync()
    loop  =  asyncio.get_event_loop ( )
    loop.create_task ( aFunc_Time(10) )
    loop.create_task ( aFunc_Uart_Tx() )
    loop.create_task ( aFunc_TelegaTx() )
    #loop.create_task ( aFunc_TelegaRxResp() )
    loop.create_task( aFunc_Uart_Rx() )
    #loop.run_forever ()
    
    from wifimgr import wlan_sta    
    app.run(debug=True, host=wlan_sta.ifconfig()[0])
